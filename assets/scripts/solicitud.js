const listElement = document.querySelector('.posts');
const postTemplate = document.getElementById('single-post');

const xhr = new XMLHttpRequest();

//No se iniciará ninguna actividad
//Primer paso para configuarar una solicitud toda al menos dos argumentos.
xhr.open('GET', 'https://jsonplaceholder.typicode.com/posts');

xhr.responseType = 'json';

//A esta propiedad le asigna una función
xhr.onload = function() {
    // console.log(xhr.response);
    //JSON.Stringify => Ayuda a convertir el código javascript a los objetos y matrices a datos JSON.
    //Parse ayuda a convertir datos JSON  de nuevo a JavaScript.
    // const listOfPosts = JSON.parse(xhr.response);
    const listOfPosts = xhr.response;
    for (const posts of listOfPosts) {
        const postEl = document.importNode(postTemplate.content, true);
        postEl.querySelector('h2').textContent = posts.title.toUpperCase();
        postEl.querySelector('p').textContent = posts.body;
        listElement.append(postEl);
    }
    console.log(listOfPosts);
}

//Enviara la solicitud
xhr.send();

